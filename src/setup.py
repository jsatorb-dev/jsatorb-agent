from cx_Freeze import setup, Executable

packages = ["os", "sys"]
excludes = ["tkinter"]
includes = ["os", "sys"]
includefiles = [ "src/jsatorb-agent.json" ]

build_exe_options = {
    "packages": packages,
    "includes": includes,
    "excludes": excludes,
    "include_files": includefiles
}

# Appel de la fonction setup
setup(
    name = "jsatorb-agent",
    version = "1",
    description = "JSatOrb agent - interface between JSatOrb GUI and the VTS visualization tool",
    options={"build_exe": build_exe_options},
    executables = [Executable("src/jsatorb-agent.py")]
)