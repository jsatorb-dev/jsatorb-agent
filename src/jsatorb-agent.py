import sys
# from os.path import join
#import os
import subprocess
import pathlib
import json
from datetime import datetime
from zipfile import ZipFile
#import inspect

# -----------------------------------------------------------------------------
# JSatOrb Agent
# -----------------------------------------------------------------------------
# This Python module is intended to be deployed as a standalone executable for 
# Linux, using the cxfreeze tool. 
# Its task is to be called by the JSatOrb Web GUI when receiving a VTS archive 
# Blob HTTP Response which has to be managed by this module.
# -----------------------------------------------------------------------------
# The module usage is edicted by the way the Web Browser calls it:
# 
# -----------------------------------------------------------------------------

# JSatOrb Agent installation folder.
# In dev environment, should be '/home/jsatorb/JSatOrb/Tools/JSatOrbAgent'
# In user environment, could be anywhere the user unzipped the JSatOrb Agent's 
# archive, but should end with the JSOA_SUFFIX.
JSOA_FOLDER = '/home/jsatorb/JSatOrb/Tools/JSatOrbAgent'
JSOA_DEPLOYED_SUFFIX = 'JSatOrbAgent'
JSOA_DEV_SUFFIX = 'jsatorb-agent/src'

VTS_ROOT_NAME = 'Vts-Linux-64bits-3.4.2'
VTS_LAUNCHER = 'startVTS'
VTS_LAUNCHER_PROJECT_CMD = '--project'

CACHE_FOLDER = 'cache'
LOG_FILENAME = 'jsatorb-agent.log'

# Run mode modify the program behaviour to adapt to the context ('DEV' or 'DEPLOYED')
RUN_MODE = 'DEPLOYED' # 'DEV'

def getLogLine(message):
    prefix = '[' + str(datetime.now()) + '] '
    return prefix + message + '\n'

def log(message):
    print(message)
    loglines = []
    loglines.append(getLogLine(message))
    logfile = pathlib.Path(JSOA_FOLDER, LOG_FILENAME)
    with logfile.open(mode='a+') as logfile:
        logfile.writelines(loglines)

def processIncomingFile(filename, homeDir):
    destFolder = pathlib.Path(JSOA_FOLDER, CACHE_FOLDER)
    log('Unzipping ' + filename + ' to folder ' + str(destFolder.as_posix()))

    zipMissionName = ''
    # Extract the VTS archive
    with ZipFile(filename, 'r') as zipObj:
        # Extract the VTS root folder from archive
        zipMissionName = zipObj.filelist[0].filename.split('/')[0]

        # Extract the Zip content
        zipObj.extractall(destFolder)

    # Launch VTS
    homeDirPath = pathlib.Path(homeDir)
    projectPath = pathlib.Path(destFolder, zipMissionName)

    vtsHomePath = None
    # DEPLOYED MODE 
    if (RUN_MODE == 'DEPLOYED'):
        vtsHomePath = pathlib.Path(homeDirPath.absolute().parent, VTS_ROOT_NAME)
    # DEVELOPMENT MODE
    if (RUN_MODE == 'DEV'):
        vtsHomePath = pathlib.Path('/home/jsatorb/JSatOrb/Tools', VTS_ROOT_NAME)

    launchVTS(projectPath, zipMissionName, vtsHomePath)

def launchVTS(projectHomePath, mission, vtsHomePath):
    log("-- Launching VTS:")
    log("    Project home path is " + str(projectHomePath.absolute()))
    log("    Mission is " + mission)
    log("    VTS home path is " + str(vtsHomePath.absolute()))
    projectPath = pathlib.Path(projectHomePath, mission + '.vts')
    vtsLauncher = pathlib.Path(vtsHomePath, VTS_LAUNCHER)
    args = [str(vtsLauncher.as_posix()), VTS_LAUNCHER_PROJECT_CMD, str(projectPath.as_posix())]
    process = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        universal_newlines=True
        )

    while True:
        output = process.stdout.readline()
        print(output.strip())
        return_code = process.poll()
        if (return_code is not None):
                print('RETURN CODE', return_code)
                # Process has finished, read the rest of the output
                for output in process.stdout.readlines():
                    print(output.strip())
                break

# Build a string explaining the Agent's usage
def usage():
    usage = '''JSatOrb Agent - interface tool between JSatOrb GUI and the VTS visualization tool
                            This tool receives compressed VTS data structures (vz files) as argument
                            from the Web browser running the JSatOrb GUI and then uncompress them before
                            giving the content to VTS, as argument
                            This cannot be done directly by the Web browser for security concerns.
            
            Usage: jsatorb-agent <home_dir> <VZ-Archive-file>
                   where    <home_dir> 
                                is the folder in which the agent is installed.
                                It cannot know itself as it is run within a Web browser, but as the tool
                                is run by the jsatorb-agent.bash script, the latter gives him this information.
                            <VZ-Archive-file>'''
    return usage

# -----------------------------------------------------------------------------
# JSatOrb Agent entry point
# -----------------------------------------------------------------------------
def runJSatOrbAgent():
    global JSOA_FOLDER # We alter the JSOA_FOLDER global variable here.
    global VTS_ROOT_NAME # We alter the VTS_ROOT_NAME global variable here.

    """This method manage the launching of the JSatOrb Agent
    and reacts to the provided command line arguments"""

    homeDir = pathlib.Path(str(sys.argv[0])).parent
    JSOA_FOLDER = homeDir.absolute() #env
    print("JSOA folder is ", JSOA_FOLDER)
    ##### FROM THIS POINT, THE LOG MECHANISM WORKS (it relies on JSOA_FOLDER)
    log("Logs are now active")

    # Load the JSatOrb Agent's configuration
    configFile = pathlib.Path(JSOA_FOLDER, "jsatorb-agent.json")
    absolutePath = configFile.absolute()

    try:
        with open(absolutePath, 'r') as configfile:
            try:            
                jsonConfig = json.load(configfile)
                VTS_ROOT_NAME = jsonConfig["VTS_folder"]
            except IOError:
                msg = "Couldn't read file JSatOrb Agent's configuration file " + absolutePath + " !"
                log(msg)
    except FileNotFoundError:
        msg = "JSatOrb Agent's configuration file " + str(absolutePath) + " doesn't exists !"
        log(msg)

    log("Arguments received: ")
    for arg in sys.argv:
        log("\t[" + arg + "]")

    # with open('/home/jsatorb/JSatOrb/Tools/JSatOrbAgent/jsatorbagent.log', 'a+') as logfile:
    #     logfile.writelines('start...')

    log('-------------- JSatOrb Agent run ----------------')

    # Check number of arguments
    if (len(sys.argv) == 2):
        receivedFilename = str(sys.argv[1])

        # Get Agent's working dir/installation dir
        workingDir = JSOA_FOLDER
        log("JSatOrb Agent's current working dir = " + str(workingDir))

        # Security check: Check the current working directory 
        # should end with 'JSatOrbAgent' in deployed mode OR
        # should end with 'jsatorb-agent/src' in development mode
        if (str(JSOA_FOLDER).endswith(JSOA_DEPLOYED_SUFFIX) or 
            str(JSOA_FOLDER).endswith(JSOA_DEV_SUFFIX)):

            # Get the command line arguments
            arguments = str(sys.argv)
            log('RECEIVING A REQUEST:')
            log(arguments)

            # Check the filename existence
            if (pathlib.Path(receivedFilename).exists()):

                log('UNCOMPRESSING THE RECEIVED FILE: ' + receivedFilename)
                processIncomingFile(receivedFilename, homeDir)
            else:
                log("Received argument file doesn't exists: " + receivedFilename + " !")
        else:
            log('JSatOrb Agent seems not to be correctly installed as its current working directory should end with ' + str(JSOA_DEPLOYED_SUFFIX) + '(user environment), or by ' + str(JSOA_DEV_SUFFIX) + '(development environment), but it is actually ' + str(JSOA_FOLDER) + ' !')
    else:
        log('ERROR: Incorrect number of arguments !')
        log(usage())

    log('-------------- JSatOrb Agent stop ---------------')

if __name__ == '__main__':
    runJSatOrbAgent()