#!/bin/bash

# Show JSatOrb Agent deployed executable's last log lines
tail -n 20 ~/JSatOrb/Tools/JSatOrbAgent/jsatorb-agent.log
