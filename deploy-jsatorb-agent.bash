#!/bin/bash

##### Deploy the the JSatOrb-Agent in its target directory

### Create the deployment directory
deploy_dir=~/JSatOrb/Tools/JSatOrbAgent

if [ ! -d "$deploy_dir" ]; then
    mkdir -p "$deploy_dir"
fi

### Copy the build content to the Tools/JSatOrbAgent folder

# Overwrite the jsatorb-agent executable
echo "Overwrite jsatorb-agent executable with the latest built"
\cp ./build/exe.linux-x86_64-3.7/jsatorb-agent "$deploy_dir"

# Overwrite the jsatorb-agent configuration
echo "Overwrite jsatorb-agent configuration with the latest built"
\cp ./build/exe.linux-x86_64-3.7/jsatorb-agent.json "$deploy_dir"

# Delete the lib folder
echo "Remove the previous lib folder"
\rm -rf "$deploy_dir"/lib

# Copy the last built lib folder
echo "Copy the last built lib folder"
cp -r ./build/exe.linux-x86_64-3.7/lib "$deploy_dir"

# Show updated folder content
echo "Deploy folder content:"
ls -al "$deploy_dir"
