#!/bin/bash

##### Test the JSatOrb Agent by running it with dummy arguments

# Go into deployment folder
cd ~/JSatOrb/Tools/JSatOrbAgent

# Run the Agent
echo "--------------------------- RUNNING JSATORB AGENT"
./jsatorb-agent DUMMY_VZ_PATH

# Go back to previous folder
cd -

# Show the log file last lines
echo "--------------------------- JSATORB AGENT LAST LOG LINES"
#tail -n 10 ~/JSatOrb/Tools/JSatOrbAgent/jsatorbagent.log
./showlog-jsatorb-agent.bash

