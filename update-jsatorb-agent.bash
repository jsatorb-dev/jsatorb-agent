#!/bin/bash

##### Build and deploy the JSatOrb Agent tool

# Build
echo "--------- BUILD JSATORB AGENT"
./build-jsatorb-agent.bash

# Deploy
echo "--------- DEPLOY JSATORB AGENT"
./deploy-jsatorb-agent.bash
