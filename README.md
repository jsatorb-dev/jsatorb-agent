# JSatOrb Agent

The JSatOrb Agent is a link between the JSatOrb GUI and the VTS visualization tool.

This little piece of software is using the Python language. It is intended to be deployed as a standalone executable for Linux using the cxfreeze tool, in order to avoid a Python installation on the user machine.
Its main task is to be called by the JSatOrb Web GUI when it receives a VTS archive Blob HTTP Response from the REST API.
So, the VTS archive is provided by the JSatOrb Web GUI to the JSatOrb Agent which uncompresses it in a local cache folder and gives the absolute path of its content as an input to the VTS space visualization tool.

All details about installing cx_Freeze, generating the JSatOrb Agent and deploying it are given [in this file](./cxfreeze-notes.md).

