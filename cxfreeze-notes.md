# JSatOrb project: Building and deploying the JSatOrb Agent

# Goal of this document

This document presents the way to install and to use the cx_Freeze tool in order to generate the JSatOrb Agent executable for the Ubuntu 18.04 platform.


# Using CX-Freeze to deploy the JSatOrb Agent

## Installing cx_Freeze on the Ubuntu 18.04 LTS Platform

Installing cx_Freeze with conda, we create a conda virtual environment with the following command:

>```conda create -n JSatOrbAgentEnv3.7 python=3.7```

We activate it:
>```conda activate JSatOrbAgentEnv3.7```

We install cx_Freeze's version 6.0 (version 6.1 has an issue):
>```conda install cx_Freeze=6.0```

Today (28/04/2020) the latest available version is Version 6.1, released January 4, 2020, but as it has an issue, we explicitely install the v6.0.

```
Collecting cx_Freeze==6.0
  Downloading cx_Freeze-6.0.tar.gz (88 kB)
     |████████████████████████████████| 88 kB 1.6 MB/s 
Building wheels for collected packages: cx-Freeze
  Building wheel for cx-Freeze (setup.py) ... done
  Created wheel for cx-Freeze: filename=cx_Freeze-6.0-cp37-cp37m-linux_x86_64.whl size=1663035 sha256=307ed4374225e2d94cf5779651bc1b615858a117f5fc412a0ba9f0280b52dc21
  Stored in directory: /home/jsatorb/.cache/pip/wheels/08/31/5f/91ad9445a55a7c00c637755ffadd89ee6769a2e6b451a890b3
Successfully built cx-Freeze
Installing collected packages: cx-Freeze
Successfully installed cx-Freeze-6.0
```

We test that it is effectively installed:
>```cxfreeze```

```
Usage: cxfreeze [options] [SCRIPT]

Freeze a Python script and all of its referenced modules to a base
executable which can then be distributed without requiring a Python
installation.

cxfreeze: error: script or a list of modules must be specified
```

## JSatOrb Agent generation

Now that cx_Freeze is installed, we can run the command:
```python src/setup.py build```

It outputs a lot of information we do not copy here, but as a proof it worked, it should have created a `build` folder containing a linux platfom specific folder in which a `jsatorb-agent` executable program and its `lib` dependencies folder should have been produced.

some convenient scripts has been written to ease the build and deploy of the JSatOrb Agent:

- **build-jsatorb-agent.bash**: Build the executable
- **deploy-jsatorb-agent.bash**: Deploy the Agent in a development environment (in the `/home/jsatorb/JSatOrb/Tools/JSatOrbAgent` dedicated folder)
- **update-jsatorb-agent.bash**: Perform the build and the deploy step presented above
- **showlog-jsatorb-agent.bash**: Display the Agent's last few log lines
- **test-jsatorb-agent.bash**: Run the Agent with dummy parameters only to check that the Agent's code is syntax error free

## JSatOrb Agent deployment

As seen in the previous paragraph, the way to deploy the Agent in a development environment is to run the `deploy-jsatorb-agent.bash` script. It copies the content of the `build/exe.linux-x86_64-3.7` folder into the `/home/jsatorb/JSatOrb/Tools/JSatOrbAgent` dedicated folder.


# Useful links

The present document has been built using the following resources

In order to select a freezing tool: [Freezing Your Code](https://docs.python-guide.org/shipping/freezing/)

In order to implement a cx_Freeze based solution (French article): [Distribuer facilement nos programmes Python avec cx_Freeze](https://openclassrooms.com/en/courses/235344-apprenez-a-programmer-en-python/235020-distribuer-facilement-nos-programmes-python-avec-cx-freeze)