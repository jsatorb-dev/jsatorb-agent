#!/bin/bash

##### Build the JSatOrb-Agent with cx_Freeze

# Necessary to export anaconda functions (see https://github.com/conda/conda/issues/7980)
#source "$CONDA_EXE/../../etc/profile.d/conda.sh"
source ~/JSatOrb/Tools/Anaconda3/etc/profile.d/conda.sh
#eval "$(conda shell.bash hook)"

# Activate the Python build environment for JSatOrb Agent
conda activate JSatOrbAgentEnv3.7

# Launch the cx_Freeze build Python script
python src/setup.py build

